package org.example.volume.repositories;

import org.example.volume.models.Volume;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VolumeRepository extends JpaRepository<Volume, Long>
{
    List<Volume> findByBookId(long bookId);
}
