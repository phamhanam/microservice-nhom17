package org.example.volume.controllers;

import lombok.AllArgsConstructor;
import org.example.volume.models.Volume;
import org.example.volume.repositories.VolumeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/volumes")
public class VolumeControl {
    @Autowired
    VolumeRepository volumeRepository;
    @PostMapping("/create")
    ResponseEntity post(@RequestBody Volume volume) {
        if (volume == null){
            return ResponseEntity.badRequest().body("Volume is required");
        }
        Volume save = volumeRepository.save(volume);
        return ResponseEntity.ok(save);
    }
    @GetMapping("/findById/{id}")
    ResponseEntity get(@PathVariable Long id) {
        if (id == null){
            return ResponseEntity.badRequest().body("Id is required");
        }
        Volume volume = volumeRepository.findById(id).get();
        return ResponseEntity.ok(volume);
    }
    @PutMapping("/update")
    ResponseEntity put(@RequestBody Volume volume) {
        Volume save = volumeRepository.save(volume);
        return ResponseEntity.ok(save);
    }
    @DeleteMapping("/delete/{id}")
    void delete(@PathVariable long id) {
        volumeRepository.deleteById(id);
    }
    @GetMapping("/getAll")
    List<Volume> getAll() {
        List<Volume> volumes = volumeRepository.findAll();
        return volumes;
    }
}
