package org.example.volume.enums;

public enum BookStatus {
    COMPLETED, IN_PROGRESS, PAUSED
}

