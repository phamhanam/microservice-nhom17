package org.example.book.controllers;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.AllArgsConstructor;
import org.example.book.models.Book;
import org.example.book.models.Chapter;
import org.example.book.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@AllArgsConstructor
@RequestMapping("/books")
public class BookController {
    @Autowired
    BookRepository bookRepository;
    @Autowired
    RedisTemplate redisTemplate;
    @Autowired
    RestTemplate restTemplate;
//    public static String  = "localhost";
    public static String HOST = "chapter";
    @GetMapping("/getChapters/{id}")
    @Retry(name = "retry1")
    @RateLimiter(name = "rateLimiter1")
//    @CircuitBreaker(name = "bookService",fallbackMethod = "bookfail")
    ResponseEntity getChapters(@PathVariable String id,@RequestHeader String token){
        System.out.println("retry run");
        if(id == null)
            return ResponseEntity.badRequest().body("Id is required");
        // check id is long
        if(!id.matches("[0-9]+"))
            return ResponseEntity.badRequest().body("Id must be a number");
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set("token", token);
        HttpEntity<String> entity = new HttpEntity<>("body", headers);

        ResponseEntity<List> exchange = restTemplate.exchange("http://"+HOST+":8083/chapters/getChapters/" + id, HttpMethod.GET, entity, List.class);
        return exchange;

    }
    @GetMapping("/getChapters2/{id}")
    @CircuitBreaker(name = "bookService",fallbackMethod = "bookfail")
    ResponseEntity getChapters2(@PathVariable String id,@RequestHeader String token){
        System.out.println("retry run");
        if(id == null)
            return ResponseEntity.badRequest().body("Id is required");
        // check id is long
        if(!id.matches("[0-9]+"))
            return ResponseEntity.badRequest().body("Id must be a number");
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set("token", token);
        HttpEntity<String> entity = new HttpEntity<>("body", headers);

        ResponseEntity<List> exchange = restTemplate.exchange("http://"+HOST+":8083/chapters/getChapters/" + id, HttpMethod.GET, entity, List.class);
        return exchange;

    }
    public  ResponseEntity bookfail( Throwable t){
        return ResponseEntity.ok("Book service is not available");
    }

    @PostMapping("/create")
    Book post(@RequestBody Book book){
        Book save = bookRepository.save(book);
        redisTemplate.opsForSet().add("books", save);
        return save;
    }
    @GetMapping("/findById/{id}")
    ResponseEntity get(@PathVariable Long id){
        if(id == null)
            return ResponseEntity.badRequest().body("Id is required");
        Book book = bookRepository.findById(id).get();
        return ResponseEntity.ok(book);
    }
    @PutMapping("/update/{id}")
    ResponseEntity put(@RequestBody Book book, @PathVariable Long id){
        if(id == null)
            return ResponseEntity.badRequest().body("Id is required");
        Book save = bookRepository.save(book);
        redisTemplate.opsForSet().remove("books", book.getId());
        redisTemplate.opsForSet().add("books", save);
        return ResponseEntity.ok(save);
    }

    @GetMapping("/search/{name}")
    ResponseEntity search(@PathVariable String name){
        if(name == null)
            return ResponseEntity.badRequest().body("Name is required");
        Set<Book> list = redisTemplate.opsForSet().members("books");
        Set<Book> result = new HashSet<>();
        for (Book book : list) {
            if(book.getName().toLowerCase().contains(name.toLowerCase())){
                result.add(book);
            }
        }
        return ResponseEntity.ok(result);
    }

    @GetMapping("/getAll")
    Set<Book> getAll(){
        Set<Book> list = redisTemplate.opsForSet().members("books");
        return list;
    }

}

