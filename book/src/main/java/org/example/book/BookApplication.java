package org.example.book;

import net.datafaker.Faker;
import org.example.book.enums.BookStatus;
import org.example.book.enums.Genre;
import org.example.book.models.*;
import org.example.book.repositories.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Array;
import java.util.*;

@SpringBootApplication
public class BookApplication {

    public static void main(String[] args) {
        SpringApplication.run(BookApplication.class, args);
    }
    @Bean
    public RestTemplate myRestTemplate() {
        return new RestTemplate();
    }
     @Bean
     CommandLineRunner initDatabase(BookRepository bookRepository,
                                    AuthorRepository authorRepository,
                                    PublisherRepository publisherRepository,
                                    VolumeRepository volumeRepository,
                                    ChapterRepository chapterRepository,
                                    RedisTemplate redisTemplate) {
         return args -> {
             Faker faker = new Faker();
             redisTemplate.delete("books");
                for (int i = 0; i < 10; i++) {
                    Author author = new Author();
                    author.setName(faker.name().firstName());
                    author.setSurname(faker.name().lastName());
                    author.setCountry(faker.address().country());
                    author.setBirthDate(faker.date().birthdayLocalDate());
                    author.setPhone(faker.phoneNumber().cellPhone());
                    author.setEmail(faker.internet().emailAddress());
                    authorRepository.save(author);

                    Publisher publisher = new Publisher();
                    publisher.setName(faker.company().name());
                    publisher.setDescription(faker.lorem().sentence(20));
                    publisher.setPhone(faker.phoneNumber().cellPhone());
                    publisher.setEmail(faker.internet().emailAddress());
                    publisher.setWebsite(faker.internet().url());
                    publisher.setAddress(faker.address().fullAddress());
                    publisherRepository.save(publisher);


                    Book book = new Book();
                    book.setName(faker.book().title());
                    book.setAuthor(author);
                    book.setLanguage(faker.address().country());
                    book.setDescription(faker.lorem().sentence(20));
                    book.setStatus(faker.options().option(BookStatus.values()));
                    book.setUrl(faker.internet().url());
                    Set<Genre> genres = new HashSet<>();
                    genres.add(faker.options().option(Genre.values()));
                    genres.add(faker.options().option(Genre.values()));
                    book.setGenre(genres);
                    bookRepository.save(book);

                    redisTemplate.opsForSet().add("books", book);

                    for (int j = 0; j < 10; j++) {
                        Volume volume = new Volume();
                        volume.setName(faker.book().title());
                        volume.setUrl(faker.internet().url());
                        volume.setCoverImage(faker.internet().url());
                        volume.setBook(book);
                        volumeRepository.save(volume);

                        for (int k = 0; k < 10; k++) {
                            Chapter chapter = new Chapter();
                            chapter.setName(faker.book().title());
                            chapter.setUrl(faker.internet().url());
                            chapter.setContent(faker.lorem().sentence(20));
                            chapter.setVolume(volume);
                            chapterRepository.save(chapter);
                        }
                    }
                }
             // get all books
             Set<Book> books = redisTemplate.opsForSet().members("books");
             books.forEach(System.out::println);

         };
     }
}
