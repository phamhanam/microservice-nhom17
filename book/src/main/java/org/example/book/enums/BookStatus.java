package org.example.book.enums;

public enum BookStatus {
    COMPLETED, IN_PROGRESS, PAUSED
}
