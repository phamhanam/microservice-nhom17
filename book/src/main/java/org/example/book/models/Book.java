package org.example.book.models;

import jakarta.persistence.*;
import lombok.*;
import org.example.book.enums.BookStatus;
import org.example.book.enums.Genre;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "books")
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Book implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    @ManyToOne
    private Author author;
    @ManyToOne
    private Publisher publisher;
    @ElementCollection(targetClass = Genre.class)
    @JoinTable(name = "genres", joinColumns = @JoinColumn(name = "book_id"))
    @Column(name = "genre", nullable = false)
    @Enumerated(EnumType.STRING)
    private Set<Genre> genre;
    private String language;
    private String description;
    @Enumerated(EnumType.STRING)
    private BookStatus status;
    private String url;

    // hashcode and equals
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        return id == ((Book) o).getId();
    }
    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

}
