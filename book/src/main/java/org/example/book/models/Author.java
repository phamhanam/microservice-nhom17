package org.example.book.models;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "authors")
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Author implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String surname;
    private String country;
    private LocalDate birthDate;
    private String phone;
    private String email;

}
