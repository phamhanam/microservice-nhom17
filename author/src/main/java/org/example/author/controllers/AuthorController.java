package org.example.author.controllers;

import lombok.AllArgsConstructor;
import org.example.author.models.Author;
import org.example.author.repositories.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/authors")
public class AuthorController {
    @Autowired
    AuthorRepository authorRepository;
    @PostMapping("/create")
    ResponseEntity post(@RequestBody Author author) {
        if (author == null) {
            return ResponseEntity.badRequest().body("Author is required");
        }
        Author save = authorRepository.save(author);
        return ResponseEntity.ok(save);
    }
    @GetMapping("/findById/{id}")
    ResponseEntity get(@PathVariable Long id) {
        if (id == null) {
            return ResponseEntity.badRequest().body("Id is required");
        }
        Author author = authorRepository.findById(id).get();
        return ResponseEntity.ok(author);
    }
    @PutMapping("/update")
    ResponseEntity put(@RequestBody Author author) {
        Author save = authorRepository.save(author);
        return ResponseEntity.ok(save);
    }
    @DeleteMapping("/delete/{id}")
    void delete(@PathVariable long id) {
        authorRepository.deleteById(id);
    }
    @GetMapping("/getAll")
    List<Author> getAll() {
        List<Author> author = authorRepository.findAll();
        return author;
    }
    @GetMapping("/test")
    String test() {
        return "test 4";
    }

}
