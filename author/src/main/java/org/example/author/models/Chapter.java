package org.example.author.models;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "chapters")
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Chapter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String url;
    private String content;
    @ManyToOne
    private Volume volume;
}

