package org.example.author.enums;

public enum BookStatus {
    COMPLETED, IN_PROGRESS, PAUSED
}

