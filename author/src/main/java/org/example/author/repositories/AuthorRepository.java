package org.example.author.repositories;

import org.example.author.models.Author;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AuthorRepository extends JpaRepository<Author, Long> {
   // List<Author> findByBookId(long bookId);
}
