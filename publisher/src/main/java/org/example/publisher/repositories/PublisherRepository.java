package org.example.publisher.repositories;

import org.example.publisher.models.Publisher;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PublisherRepository extends JpaRepository<Publisher, Long> {
    //List<Publisher> findByBookId(long bookId);
}
