package org.example.publisher.controllers;

import lombok.AllArgsConstructor;
import org.example.publisher.models.Publisher;
import org.example.publisher.repositories.PublisherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/publishers")
public class PublisherControllers {
    @Autowired
    PublisherRepository publisherRepository;
    @PostMapping("/create")
    ResponseEntity post(@RequestBody Publisher publisher) {
        if (publisher == null){
            return ResponseEntity.badRequest().body("Publisher is required");
        }
        Publisher save = publisherRepository.save(publisher);
        return ResponseEntity.ok(save);
    }
    @GetMapping("/findById/{id}")
    ResponseEntity get(@PathVariable Long id) {
        if (id == null){
            return ResponseEntity.badRequest().body("Id is required");
        }
        Publisher publisher = publisherRepository.findById(id).get();
        return ResponseEntity.ok(publisher);
    }
    @PutMapping("/update")
    ResponseEntity put(@RequestBody Publisher publisher) {
        Publisher save = publisherRepository.save(publisher);
        return ResponseEntity.ok(save);
    }
    @DeleteMapping("/delete/{id}")
    void delete(@PathVariable long id) {
        publisherRepository.deleteById(id);
    }
    @GetMapping("/getAll")
    List<Publisher> getAll() {
        List<Publisher> publishers = publisherRepository.findAll();
        return publishers;
    }

}
