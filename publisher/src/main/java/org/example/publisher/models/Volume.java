package org.example.publisher.models;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "volumes")
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Volume {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String url;
    private String coverImage;
    @ManyToOne
    private Book book;
}
