package org.example.publisher.enums;

public enum Genre {
    FICTION,
    NON_FICTION,
    SCIENCE_FICTION,
    MYSTERY,
    HORROR,
    THRILLER,
    ROMANCE,
    WESTERN,
    DYSTOPIAN,
    MEMOIR,
    BIOGRAPHY,
    AUTOBIOGRAPHY,
    PLAY,
    POETRY,
    FAIRY_TALE,
    FANTASY,
    MYTHOLOGY,
    LEGEND,
    FABLE,
    FOLKLORE,
    FAIRY_STORY,
    HISTORICAL_FICTION,
    SCIENCE_FANTASY,
    SATIRE,
    PARODY,
    TRAGEDY,
    COMEDY,
    DRAMA,
    EPIC,
    LYRIC,
    PROSE,
    NOVEL,
    SHORT_STORY,
    NOVELLA,
    NOVELETTE,
    ESSAY,
    ARTICLE,
    SPEECH,
    LETTER,
    DIARY,
    JOURNAL,
    BLOG,
    MEMO,
    AUTOBIOGRAPHICAL_NOVEL,
    BIOGRAPHICAL_NOVEL,
    HISTORICAL_NOVEL,
    ROMANTIC_NOVEL,
    PSYCHOLOGICAL_NOVEL,
    POLITICAL_NOVEL,
    SCIENCE_FICTION_NOVEL,
    FANTASY_NOVEL,
    MYSTERY_NOVEL,
    CRIME_NOVEL,
    DETECTIVE_NOVEL,
    THRILLER_NOVEL,
    HORROR_NOVEL,
    WESTERN_NOVEL,
    ADVENTURE_NOVEL,
    SPY_FICTION,
    YOUNG_ADULT_FICTION,
    CHILDREN_FICTION,
    COMING_OF_AGE_STORY,
    SLICE_OF_LIFE_STORY,
    DYSTOPIAN_NOVEL,
    UTOPIAN_NOVEL,
    TIME_TRAVEL_NOVEL,
    ALTERNATE_HISTORY,
    MILITARY_FICTION,
    MILITARY_SCIENCE_FICTION,
    MILITARY_FANTASY,
    MILITARY_HISTORY,
    MILITARY_MEMOIR,
    MILITARY_BIOGRAPHY,
    MILITARY_AUTOBIOGRAPHY,
    MILITARY_NOVEL,
    MILITARY_ROMANCE,
    MILITARY_THRILLER,
    MILITARY_HORROR,
    MILITARY_WESTERN,
    MILITARY_DYSTOPIAN,
}
