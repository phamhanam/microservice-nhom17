package com.example.auth.utils;

import com.example.auth.models.User;

public class UserValidate {
//    private static String REGEX_PASSWORD = "(?=.*[A-Za-z])(?=.*[0-9])[A-Za-z0-9]{3,10}";
    private static String REGEX_PASSWORD = "^[A-Za-z0-9]{3,10}$";
    public static boolean validate(User user){
        // contains must letters and numbers, at least 3 characters long, maximum 10 characters long
        return user.getUsername().matches(REGEX_PASSWORD);
    }
}
