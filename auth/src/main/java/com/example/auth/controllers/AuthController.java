package com.example.auth.controllers;

import com.example.auth.models.User;

import com.example.auth.models.UserDTO;
import com.example.auth.services.AuthService;
import com.example.auth.services.UserService;
import com.example.auth.utils.Jwt;
import com.example.auth.utils.UserValidate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    AuthService authService;
    @Autowired
    UserService userService;

    @PostMapping("/register")
    public ResponseEntity register(@RequestBody User user) {
        if (!UserValidate.validate(user))
            return ResponseEntity.badRequest().body("validate password failed");
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        User saveUser = authService.register(user);
        return ResponseEntity.ok(saveUser);
    }

    @GetMapping("/login")
    public ResponseEntity login(@RequestBody User user) {
        User myUser = userService.getMyUserFromUsername(user.getUsername());
        if (myUser == null)
            return ResponseEntity.badRequest().body("Username or password is incorrect");
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

        if (!bCryptPasswordEncoder.matches(user.getPassword(), myUser.getPassword()))
            return ResponseEntity.badRequest().body("Username or password is incorrect");

        String token = Jwt.generateToken(user);
        UserDTO userDTO = new UserDTO(myUser, token);
        return ResponseEntity.ok(userDTO);
    }

    @GetMapping("/checkToken/{token}")
    public boolean checkToken(@PathVariable String token) {
        System.out.println(token);
        return !Jwt.isTokenExpired(token);
    }
    @GetMapping("/hello")
    public String hello(@RequestHeader(name = "token") String token) {
        if (Jwt.isTokenExpired(token))
            return "Token expired";
        return "hello";
    }
}
