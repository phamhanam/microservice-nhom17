package com.example.auth.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public class UserDTO extends User{
    private String token;
    public UserDTO(User user, String token) {
        super(user.getUsername(), user.getPassword(), user.getRole());
        this.token = token;
    }
}
