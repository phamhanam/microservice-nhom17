package com.example.auth.services;

import com.example.auth.models.User;
import com.example.auth.repositories.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserService {
    @Autowired
    UserRepository userRepository;

    public User getMyUserFromUsername(String username) {
        return userRepository.findByUsername(username);
    }
}
