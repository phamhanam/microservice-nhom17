package com.example.auth.services;

import com.example.auth.models.User;
import com.example.auth.repositories.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AuthService {
    @Autowired
    UserRepository userRepository;

    public User register(User user) {
        return userRepository.save(user);
    }
}
