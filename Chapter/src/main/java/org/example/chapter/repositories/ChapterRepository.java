package org.example.chapter.repositories;

import org.example.chapter.models.Chapter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ChapterRepository extends JpaRepository<Chapter, Long> {
    List<Chapter> findByVolumeId(long volumeId);
    @Query("SELECT c FROM Chapter c WHERE c.volume.book.id = :bookId")
    List<Chapter> getChaptersByVolume_Book_Id(long bookId);
}
