package org.example.chapter.controllers;

import lombok.AllArgsConstructor;
import org.example.chapter.models.Chapter;
import org.example.chapter.repositories.ChapterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@RequestMapping("/chapters")
@RestController
public class ChapterController {
    @Autowired
    ChapterRepository chapterrepository;
    @Autowired
    RestTemplate restTemplate;
//    public static String  HOST = "localhost";
    public static String  HOST = "auth";
    @GetMapping("/getChapters/{bookId}")
    ResponseEntity getChapters(@PathVariable Long bookId
            , @RequestHeader("token") String token
           ) {
        if(token.isBlank()){
            return ResponseEntity.badRequest().body("Token is required in header");
        }
//        else {
//            return ResponseEntity.ok(token.get());
//        }
        Boolean token1 = restTemplate.getForObject("http://"+HOST+":8081/auth/checkToken/"+ token, Boolean.class);
        if(token1 == false){
            return ResponseEntity.badRequest().body("Token is expired");
        }
        System.out.println("bookId: " + bookId);
        List<Chapter> chapters = chapterrepository.getChaptersByVolume_Book_Id(bookId);
        return ResponseEntity.ok(chapters);
    }
    @PostMapping("/create")
    ResponseEntity post(@RequestBody Chapter chapter , @RequestHeader String token) {
        Boolean token1 = restTemplate.getForObject("http://"+HOST+":8081/auth/checkToken/"+token, Boolean.class);
        if(token1 == false) {
            return ResponseEntity.badRequest().body("Token is expired");
        }
        if (chapter == null){
            return ResponseEntity.badRequest().body("Chapter is required");
        }
        Chapter save = chapterrepository.save(chapter);
        return ResponseEntity.ok(save);
    }
    @GetMapping("/findById/{id}")
    ResponseEntity get(@PathVariable Long id , @RequestHeader String token) {
        Boolean token1 = restTemplate.getForObject("http://"+HOST+":8081/auth/checkToken/"+token, Boolean.class);
        if(token1 == false) {
            return ResponseEntity.badRequest().body("Token is expired");
        }
        if (id == null){
            return ResponseEntity.badRequest().body("Id is required");
        }
        Chapter chapter = chapterrepository.findById(id).get();
        return ResponseEntity.ok(chapter);
    }
    @PutMapping("/update")
    ResponseEntity put(@RequestBody Chapter chapter , @RequestHeader String token) {
        Boolean token1 = restTemplate.getForObject("http://"+HOST+":8081/auth/checkToken/"+token, Boolean.class);
        if(token1 == false) {
            return ResponseEntity.badRequest().body("Token is expired");
        }
        Chapter save = chapterrepository.save(chapter);
        return ResponseEntity.ok(save);
    }
    @DeleteMapping("/delete/{id}")
    void delete(@PathVariable Long id ) {
        chapterrepository.deleteById(id);
    }
    @GetMapping("/getAll")
    ResponseEntity getAll(@RequestHeader String token) {
        Boolean token1 = restTemplate.getForObject("http://"+HOST+":8081/auth/checkToken/"+token, Boolean.class);
        if(token1 == false) {
            return ResponseEntity.badRequest().body("Token is expired");
        }
        List<Chapter> chapter = chapterrepository.findAll();
        return ResponseEntity.ok(chapter);
    }
}
