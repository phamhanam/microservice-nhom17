package org.example.chapter.models;

import jakarta.persistence.*;
import lombok.*;
import org.example.chapter.enums.BookStatus;
import org.example.chapter.enums.Genre;

import java.util.Set;

@Entity
@Table(name = "books")
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    @ManyToOne
    private Author author;
    @ManyToOne
    private Publisher publisher;
    @ElementCollection(targetClass = Genre.class)
    @JoinTable(name = "genres", joinColumns = @JoinColumn(name = "book_id"))
    @Column(name = "genre", nullable = false)
    @Enumerated(EnumType.STRING)
    private Set<Genre> genre;
    private String language;
    private String description;
    @Enumerated(EnumType.STRING)
    private BookStatus status;
    private String url;
}
