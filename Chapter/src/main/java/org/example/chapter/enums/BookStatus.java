package org.example.chapter.enums;

public enum BookStatus {
    COMPLETED, IN_PROGRESS, PAUSED
}

